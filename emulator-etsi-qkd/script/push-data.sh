#!/bin/bash 

pkill -f sysrepo
pkill -f sysrepod
pkill -f example_application
pkill -f netopeer2-server


export LD_LIBRARY_PATH=/lib:/usr/lib:/usr/local/lib

# Clean the yang space
# Not required with ETSI - keep it for reference 
# sysrepoctl --uninstall --module=openconfig-terminal-device --revision=2017-07-08

# Change a leafref to leaf with type uint32, because of parsing errors
# Not required with ETSI - keep it for reference 
# sed -i '487,491d' openconfig-terminal-device.yang
# sed -i '486a type uint32;' openconfig-terminal-device.yang

# Modify operational schema into configurable schema
# Not required with ETSI - keep it for reference 
# sed -i '/config false;/d' openconfig-platform-transceiver.yang
# sed -i '/config false;/d' openconfig-platform.yang
# sed -i '/config false;/d' openconfig-terminal-device.yang

# Install ETSI yang files including initial datastore
echo "--- [push-data.sh] installing ETSI yang modules including initial datastore..."
sysrepoctl --install /root/yang/etsi-qkd-node-types.yang --owner root --group root --permissions 666
sysrepoctl --install /root/yang/etsi-qkd-sdn-node.yang --init-data /root/config/init-etsi-qkd-sdn-node.xml --owner root --group root --permissions 666

#Subscribe sysrepo to etsi-node changes
echo "--- [push-data.sh] sysrepo subscribe to etsi-qkd-sdn-node changes"
/root/sysrepo/build/examples/application_changes_example etsi-qkd-sdn-node &
sleep 4

# Start Netconf Server
echo "--- [push-data.sh] starting netopeer2-server..."
netopeer2-server
sleep 5

# start rest server
python3 /root/python_rest/server.py

tail -f

